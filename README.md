# Avestruz

A big, clumsy but gentle typeface for the right dose of silliness and stature.

Made in the [Type:Bits workshop](https://typebits.gitlab.io) in Tomelloso, 2016.

![Font preview](https://gitlab.com/typebits/font-avestruz/-/jobs/artifacts/master/raw/Avestruz-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-avestruz/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-avestruz/-/jobs/artifacts/master/raw/Avestruz-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-avestruz/-/jobs/artifacts/master/raw/Avestruz-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-avestruz/-/jobs/artifacts/master/raw/Avestruz-Regular.sfd?job=build-font)

# Authors

* Javier M. Arenas
* Juan Fran M.A.
* Elisa G. Godoy
* Guillermo Martinez
* Pablo
* Pedro V. Valero
* Estanislao S. Serrano

# License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
